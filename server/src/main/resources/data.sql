INSERT INTO categories (name)
VALUES
  ('משכנתא הרא"ה'),
  ('משכנתא כפ"ס'),
  ('שיפוצים'),
  ('אשראי'),
  ('משכורת'),
  ('ביטוח בריאות'),
  ('נסיעות'),
  ('החזר הלוואות'),
  ('פקדונות'),
  ('גן ילדים'),
  ('בטוח לאומי/קצבה'),
  ('עמלות בנק'),
  ('לקיחת הלוואות'),
  ('CashBack'),

  ('ביגוד'),
  ('אוכל'),
  ('הוצאות לבית');

INSERT INTO category_mappings (category_id, reference_pattern, comment_pattern, min_amount, max_amount)
VALUES
  -- Mortgage Haroeh
  (1, '26242529', NULL, NULL, NULL),

  -- Mortgate Kfar Saba
  (2, '42209635', NULL, NULL, NULL),
  (2, '704528', NULL, NULL, NULL),

  -- Building renovations Maapiley Egoz
  (3, '36056653', NULL, NULL, NULL),

  -- Salary
  (5, '72816010', NULL, NULL, NULL),
  (5, '16627010', NULL, NULL, NULL),
  (5, '99013330', NULL, NULL, NULL),
  (5, '84182203', NULL, NULL, NULL),
  (5, NULL, '.*\Qשיק במכונה\E.*', 1, NULL),

  -- Credit Cards bulk charge
  (14, NULL, '.*\QkcaBhsaC\E.*', 0, NULL),
  (4, '20543997', NULL, NULL, NULL),
  (4, '4959', NULL, NULL, NULL),
  (4, '9374', '.*\Qמשיכה מבנקט\E.*', NULL, NULL),
  (4, '9374', NULL, NULL, NULL),
  (4, '6888', NULL, NULL, NULL),

  -- Health Insurance
  (6, '628545', NULL, NULL, NULL),
  (6, '7377518', NULL, NULL, NULL),

  -- Driving expenses
  (7, '42321653', NULL, NULL, NULL),

  -- Pay loans
  (8, '684397811', NULL, NULL, 0),
  (8, NULL, 'רבית', -2000, -1),

  -- Saving deposits
  (9, '912684397811', NULL, NULL, NULL),

  -- Kindergarden
  (10, NULL, 'שיק', -3150, -3150),
  (10, NULL, 'שיק', -3170, -3170),
  (10, NULL, 'שיק', -3100, -3100),
  (10, '10450', 'שיק', -1200, -1200),

  -- Social Security refunds, children allowances, etc
  (11, '63051080', NULL, NULL, NULL),
  (11, '13104088', NULL, NULL, NULL),

  -- Bank commissions
  (12, NULL, '.*\Qע.מפעולות\E.*', NULL, NULL),

  -- Taking loans
  (13, '684397811', NULL, 0, NULL),

  -- Non-existing mapping, for easier editing of the above
  (10, 'lalalalala', NULL, NULL, NULL);

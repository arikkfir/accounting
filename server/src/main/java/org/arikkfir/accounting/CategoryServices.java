package org.arikkfir.accounting;

import org.arikkfir.accounting.framework.persistence.ReadOnly;
import org.arikkfir.accounting.framework.persistence.ReadWrite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.net.URI;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * @author arik
 */
@RestController
@RequestMapping( "/api/categories" )
public class CategoryServices
{
    @Nonnull
    private final Map<String, Pattern> compiledPatterns = new ConcurrentHashMap<>();

    @Nonnull
    private final Dao dao;

    @Autowired
    public CategoryServices( @Nonnull Dao dao )
    {
        this.dao = dao;
    }

    @RequestMapping( method = GET )
    @ReadOnly
    public Collection<Category> getAll( @Nullable @RequestParam( required = false ) String name )
    {
        if( name != null )
        {
            return this.dao.findAllCategoriesByName( "%" + name + "%" );
        }
        else
        {
            return this.dao.findAllCategories();
        }
    }

    @RequestMapping( method = GET, value = "/{id}" )
    @ReadOnly
    public ResponseEntity<?> getById( @PathVariable int id )
    {
        try
        {
            return ResponseEntity.ok( this.dao.findCategoryById( id ) );
        }
        catch( EmptyResultDataAccessException e )
        {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping( method = POST )
    @ReadWrite
    public ResponseEntity<Void> create( @Nonnull @RequestParam String name )
    {
        Category category = this.dao.createCategory( name );
        return ResponseEntity.created( URI.create( "/api/categories/" + category.getId() ) ).build();
    }

    @RequestMapping( method = DELETE, value = "/{id}" )
    @ReadWrite
    public ResponseEntity<Void> delete( @PathVariable int id )
    {
        try
        {
            this.dao.findCategoryById( id ).delete();
            return ResponseEntity.noContent().build();
        }
        catch( EmptyResultDataAccessException e )
        {
            return ResponseEntity.notFound().build();
        }
    }

    @Nullable
    @ReadOnly
    public Category findAppropriateCategory( @Nonnull BigDecimal amount,
                                             @Nullable String reference,
                                             @Nullable String comment )
    {
        for( CategoryMapping mapping : dao.findAllCategoryMappings() )
        {
            // compare minimum amount
            BigDecimal minAmount = mapping.getMinAmount();
            if( minAmount != null && amount.compareTo( minAmount ) < 0 )
            {
                continue;
            }

            // compare maximum amount
            BigDecimal maxAmount = mapping.getMaxAmount();
            if( maxAmount != null && amount.compareTo( maxAmount ) > 0 )
            {
                continue;
            }

            // compare reference
            String referencePattern = mapping.getReferencePattern();
            if( reference != null && referencePattern != null )
            {
                if( !getPattern( referencePattern ).matcher( reference ).matches() )
                {
                    continue;
                }
            }

            // compare comment
            String commentPattern = mapping.getCommentPattern();
            if( comment != null && commentPattern != null )
            {
                if( !getPattern( commentPattern ).matcher( comment ).matches() )
                {
                    continue;
                }
            }

            return mapping.getCategory();
        }
        return null;
    }

    @Nonnull
    private Pattern getPattern( @Nonnull String pattern )
    {
        return this.compiledPatterns.computeIfAbsent( pattern, Pattern::compile );
    }
}

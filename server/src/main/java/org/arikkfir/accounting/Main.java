package org.arikkfir.accounting;

import ch.qos.logback.classic.Level;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.logging.LoggingApplicationListener;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

import static java.lang.System.getProperty;
import static java.nio.file.Files.exists;

/**
 * @author arik
 */
public class Main
{
    public static void main( String[] args ) throws IOException
    {
        // change Spring Boot application name to "accounting"
        System.setProperty( "spring.config.name", "accounting" );

        // determine/set application home
        Path applicationHome = Paths.get( getProperty( "application.home", getProperty( "user.dir" ) ) );
        System.setProperty( "application.home", applicationHome.toString() );

        // configure logging
        configureLogging( applicationHome );

        // run!
        try
        {
            new AccountingSpringApplication().run( args );
        }
        catch( Throwable ignore )
        {
            // SpringApplication.run(...) already logs startup errors
        }
    }

    private static void configureLogging( Path applicationHome ) throws IOException
    {
        //
        // install bridge from 'java.util.logging' to SLF4J
        //
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();

        //
        // load (if exists) the 'conf/loggers.properties' file, to customize logger levels
        //
        Path loggersFile = applicationHome.resolve( "conf/loggers.properties" );
        if( exists( loggersFile ) )
        {
            Properties properties = new Properties();
            try( InputStream inputStream = Files.newInputStream( loggersFile ) )
            {
                properties.load( inputStream );
                for( String loggerName : properties.stringPropertyNames() )
                {
                    String levelValue = properties.getProperty( loggerName );
                    Logger logger = LoggerFactory.getLogger( loggerName );
                    if( logger instanceof ch.qos.logback.classic.Logger )
                    {
                        ch.qos.logback.classic.Logger lbLogger = ( ch.qos.logback.classic.Logger ) logger;
                        lbLogger.setLevel( Level.valueOf( levelValue ) );
                    }
                }
            }
        }
    }

    private static class AccountingSpringApplication extends SpringApplication
    {
        private AccountingSpringApplication()
        {
            super( Accounting.class );

            //
            // we want to remove the default Spring Boot LOGGING initializer and replace it with our own
            ///
            Set<ApplicationListener<?>> initializers = new LinkedHashSet<>( getListeners() );
            for( Iterator<ApplicationListener<?>> iterator = initializers.iterator(); iterator.hasNext(); )
            {
                if( iterator.next() instanceof LoggingApplicationListener )
                {
                    iterator.remove();
                    break;
                }
            }
            setListeners( initializers );
        }

        @Override
        protected void afterRefresh( ConfigurableApplicationContext context, String[] args )
        {
            super.afterRefresh( context, args );

            Accounting application = context.getBean( Accounting.class );
            application.setApplicationContext( context );

            Accounting.setApplication( application );

            context.publishEvent( new Accounting.ApplicationInitializedEvent( context ) );
        }
    }
}

package org.arikkfir.accounting;

import org.arikkfir.dao.Column;
import org.arikkfir.dao.Delete;
import org.arikkfir.dao.Id;
import org.arikkfir.dao.Table;

/**
 * @author arik
 */
@Table( "categories" )
public interface Category
{
    @Id
    @Column( "id" )
    int getId();

    @Column( "name" )
    String getName();

    @Delete
    void delete();
}

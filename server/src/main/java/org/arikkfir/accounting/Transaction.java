package org.arikkfir.accounting;

import java.math.BigDecimal;
import java.time.LocalDate;
import org.arikkfir.dao.Column;
import org.arikkfir.dao.Id;
import org.arikkfir.dao.Table;

/**
 * @author arik
 * @on 1/17/15.
 */
@Table( "transactions" )
public interface Transaction
{
    @Id
    @Column( "id" )
    int getId();

    @Column( "category_id" )
    Category getCategory();

    @Column( "transaction_date" )
    LocalDate getTransactionDate();

    @Column( "amount" )
    BigDecimal getAmount();

    @Column( "reference" )
    String getReference();

    @Column( "comment" )
    String getComment();
}

package org.arikkfir.accounting;

import org.arikkfir.dao.Query;
import org.springframework.jdbc.core.RowMapper;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;

/**
 * @author arik
 */
public interface Dao
{
    @Nonnull
    Category createCategory( @Nonnull String name );

    @Nonnull
    @Query( "SELECT id FROM categories WHERE id = :id " )
    Category findCategoryById( int id );

    @Query( "SELECT id FROM categories ORDER BY name" )
    @Nonnull
    List<Category> findAllCategories();

    @Nonnull
    @Query( "SELECT id FROM categories WHERE name LIKE :name " )
    List<Category> findAllCategoriesByName( @Nonnull String name );

    @Query( "SELECT id FROM category_mappings ORDER BY id" )
    @Nonnull
    List<CategoryMapping> findAllCategoryMappings();

    @Nonnull
    Transaction create( @Nullable Integer category,
                        @Nonnull String reference,
                        @Nonnull LocalDate transactionDate,
                        @Nonnull BigDecimal amount,
                        @Nullable String comment );

    @Nonnull
    @Query( "SELECT id FROM transactions WHERE id = :id " )
    Transaction findById( int id );

    @Nullable
    @Query( "SELECT id " +
            "FROM   transactions " +
            "WHERE  category_id IS NOT DISTINCT FROM CAST(:categoryId AS INTEGER) " +
            "AND    reference IS NOT DISTINCT FROM CAST(:reference AS VARCHAR(255)) " +
            "AND    transaction_date IS NOT DISTINCT FROM CAST(:transactionDate AS DATE) " +
            "AND    amount IS NOT DISTINCT FROM CAST(:amount AS DECIMAL) " +
            "AND    comment IS NOT DISTINCT FROM CAST(:comment AS VARCHAR(2048)) " )
    Transaction findDuplicateTransaction( @Nullable Integer categoryId,
                                          @Nullable String reference,
                                          @Nonnull LocalDate transactionDate,
                                          @Nonnull BigDecimal amount,
                                          @Nullable String comment );

    @Query( "SELECT id FROM transactions ORDER BY transaction_date ASC " )
    @Nonnull
    List<Transaction> findAll();

    @Nonnull
    @Query( "SELECT id FROM transactions WHERE transaction_date >= :from ORDER BY transaction_date DESC " )
    List<Transaction> findFromDate( @Nonnull LocalDate from );

    @Nonnull
    @Query( "SELECT id FROM transactions WHERE transaction_date <= :until ORDER BY transaction_date DESC " )
    List<Transaction> findUntilDate( @Nonnull LocalDate until );

    @Nonnull
    @Query( "SELECT SUM( amount ) AS balance FROM transactions WHERE transaction_date <= :date " )
    BigDecimal findBalanceForDate( @Nonnull LocalDate date );

    @Nonnull
    @Query( "SELECT id FROM transactions WHERE transaction_date >= :from AND transaction_date <= :until ORDER BY transaction_date DESC " )
    List<Transaction> findBetweenDates( @Nonnull LocalDate from, @Nonnull LocalDate until );

    @Nonnull
    @Query( value = "SELECT     YEAR( t.transaction_date ) AS year," +
                    "           MONTH( t.transaction_date ) AS month," +
                    "           t.category_id AS category_id," +
                    "           c.name AS category_name," +
                    "           SUM( t.amount ) AS amount " +
                    "FROM       transactions AS t " +
                    "JOIN       categories AS c ON c.id = t.category_id " +
                    "WHERE      t.transaction_date >= :from AND t.transaction_date <= :until " +
                    "AND        t.category_id IS NOT NULL " +
                    "GROUP BY   YEAR( t.transaction_date )," +
                    "           MONTH( t.transaction_date )," +
                    "           t.category_id," +
                    "           c.name " +
                    "HAVING     ABS(SUM( t.amount )) >= :threshold " +
                    "ORDER BY   YEAR( t.transaction_date )  ASC, " +
                    "           MONTH( t.transaction_date ) ASC ",
            rowMapper = MonthCategoryAmountRowMapper.class )
    List<MonthCategoryAmount> selectExpensesAndIncomeByCategories( @Nonnull LocalDate from,
                                                                   @Nonnull LocalDate until,
                                                                   @Nonnull BigDecimal threshold );

    class MonthCategoryAmountRowMapper implements RowMapper<MonthCategoryAmount>
    {
        @Override
        public MonthCategoryAmount mapRow( ResultSet rs, int rowNum ) throws SQLException
        {
            return new MonthCategoryAmount( YearMonth.of( rs.getInt( "year" ), rs.getInt( "month" ) ),
                                            rs.getInt( "category_id" ),
                                            rs.getString( "category_name" ),
                                            rs.getBigDecimal( "amount" ) );
        }
    }

    class MonthCategoryAmount
    {
        @Nonnull
        private final YearMonth yearAndMonth;

        private final int categoryId;

        @Nonnull
        private final String categoryName;

        @Nonnull
        private final BigDecimal amount;

        private MonthCategoryAmount( @Nonnull YearMonth yearAndMonth,
                                     int categoryId,
                                     @Nonnull String categoryName,
                                     @Nonnull BigDecimal amount )
        {
            this.yearAndMonth = yearAndMonth;
            this.categoryId = categoryId;
            this.categoryName = categoryName;
            this.amount = amount;
        }

        @Nonnull
        public YearMonth getYearAndMonth()
        {
            return this.yearAndMonth;
        }

        public int getCategoryId()
        {
            return this.categoryId;
        }

        @Nonnull
        public String getCategoryName()
        {
            return this.categoryName;
        }

        @Nonnull
        public BigDecimal getAmount()
        {
            return this.amount;
        }
    }
}

package org.arikkfir.accounting.util;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author arik
 */
public final class Property
{
    @Nonnull
    public static Property p( @Nonnull String name, @Nullable Object value )
    {
        return new Property( name, value );
    }

    @Nonnull
    private final String name;

    @Nullable
    private final Object value;

    private Property( @Nonnull String name, @Nullable Object value )
    {
        this.name = name;
        this.value = value;
    }

    @Nonnull
    public String getName()
    {
        return this.name;
    }

    @Nullable
    public Object getValue()
    {
        return this.value;
    }
}

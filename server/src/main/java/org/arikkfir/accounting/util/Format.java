package org.arikkfir.accounting.util;

import javax.annotation.Nonnull;
import org.slf4j.helpers.MessageFormatter;

/**
 * @author arik
 */
public final class Format
{
    @Nonnull
    public static String msg( @Nonnull String pattern, @Nonnull Object... args )
    {
        return MessageFormatter.arrayFormat( pattern, args ).getMessage();
    }

    private Format()
    {
    }
}

package org.arikkfir.accounting.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import javax.annotation.Nonnull;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import static org.arikkfir.accounting.util.Format.msg;

/**
 * @author arik
 */
@Aspect
public class NullabilityAdvice
{
    @Around( "execution( * *.*(..,@javax.annotation.Nonnull (*),..) )" )
    public Object rejectNullArgumentsForNonnullMethodParameters( @Nonnull ProceedingJoinPoint joinPoint )
            throws Throwable
    {
        Object[] args = joinPoint.getArgs();

        Signature signature = joinPoint.getSignature();
        if( signature instanceof MethodSignature )
        {
            MethodSignature methodSignature = ( MethodSignature ) signature;
            Method method = methodSignature.getMethod();
            Annotation[][] parameterAnnotations = method.getParameterAnnotations();
            for( int i = 0; i < parameterAnnotations.length; i++ )
            {
                Object argValue = args[ i ];

                Annotation[] paramAnns = parameterAnnotations[ i ];
                for( Annotation ann : paramAnns )
                {
                    if( Nonnull.class.equals( ann.annotationType() ) )
                    {
                        if( argValue == null )
                        {
                            throw new NullPointerException( msg( "parameter '{}' should not be null (for method '{}')",
                                                                 methodSignature.getParameterNames()[ i ],
                                                                 signature.toLongString() ) );
                        }
                    }
                }
            }
        }

        return joinPoint.proceed();
    }

    @Around( "execution( @javax.annotation.Nonnull * *.*(..) )" )
    public Object rejectNullReturnValueForNonnullMethods( @Nonnull ProceedingJoinPoint joinPoint ) throws Throwable
    {
        Object returnValue = joinPoint.proceed();
        if( returnValue == null )
        {
            throw new NullPointerException( msg( "method '{}' should not return null", joinPoint.getSignature().toLongString() ) );
        }
        else
        {
            return returnValue;
        }
    }
}

package org.arikkfir.accounting;

import org.arikkfir.accounting.framework.persistence.ReadWrite;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static java.time.temporal.ChronoField.*;
import static org.apache.commons.lang3.StringUtils.reverse;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.util.StringUtils.trimLeadingCharacter;

/**
 * @author arik
 */
@Component
public class BankTransactionsLoader
{
    private static final Logger LOG = getLogger( BankTransactionsLoader.class );

    @Nonnull
    private final DateTimeFormatter dayMonthAndShortYearFormatter = DateTimeFormatter.ofPattern( "dd/MM/yy" );

    @Nonnull
    private final DecimalFormat decimalFormat;

    @Nonnull
    private final Dao dao;

    @Nonnull
    private final CategoryServices categoryServices;

    @Autowired
    public BankTransactionsLoader( @Nonnull Dao Dao, @Nonnull CategoryServices categoryServices )
    {
        this.decimalFormat = new DecimalFormat( "###,###.##" );
        this.decimalFormat.setGroupingSize( 3 );
        this.decimalFormat.setGroupingUsed( true );
        this.decimalFormat.setParseBigDecimal( true );

        this.dao = Dao;
        this.categoryServices = categoryServices;
    }

    @Nullable
    private static String extractText( @Nonnull Element element )
    {
        return !element.hasText() ? null : extractText( element.text() );
    }

    @Nonnull
    private static String requireText( @Nonnull Element element )
    {
        return requireText( element.text() );
    }

    @Nullable
    private static String extractText( @Nullable String text )
    {
        if( text == null )
        {
            return null;
        }
        else
        {
            text = requireText( text );
            return text.isEmpty() ? null : text;
        }
    }

    @Nonnull
    private static String requireText( @Nonnull String text )
    {
        return text.replace( ( char ) 160, ' ' ).trim();
    }

    @ReadWrite
    public void loadBankHapoalimFile( @Nonnull Path path )
    {
        Element txTable = parseDocument( path ).body().getElementById( "mytable_body" );
        if( txTable == null )
        {
            throw new TransactionsParseException( "the BankHapoalim file at '" + path + "' is in unknown format (could not find table 'mytable_body')" );
        }

        AtomicBoolean resetBalance = new AtomicBoolean( this.dao.findAll().isEmpty() );

        AtomicInteger count = new AtomicInteger( 0 );
        txTable.getElementsByClass( "TR_ROW_BANKTABLE" )
               .stream()
               .filter( rowElement -> rowElement.getElementsByTag( "td" ).size() == 6 )
               .map( trElement -> {
                   Elements tdElements = trElement.getElementsByTag( "td" );
                   return new TxRow( tdElements.get( 0 ),
                                     tdElements.get( 1 ),
                                     tdElements.get( 2 ),
                                     tdElements.get( 3 ),
                                     tdElements.get( 4 ),
                                     tdElements.get( 5 ) );
               } )
               .filter( row -> {
                   Transaction duplicate = this.dao.findDuplicateTransaction( row.getCategoryId(),
                                                                              row.getReference(),
                                                                              row.getDate(),
                                                                              row.getAmount(),
                                                                              row.getComment() );
                   return duplicate == null;
               } )
               .forEachOrdered( row -> {
                   if( resetBalance.get() )
                   {
                       this.dao.create( null, "00000000", row.getDate().minusDays( 1 ), row.getBalanceAfterTx().subtract( row.amount ), "איזון עו\"ש" );
                       resetBalance.set( false );
                   }
                   this.dao.create( row.getCategoryId(), row.getReference(), row.getDate(), row.getAmount(), row.getComment() );
                   count.incrementAndGet();
               } );
        LOG.info( "Loaded {} new transactions", count.get() );
    }

    @ReadWrite
    public void loadBankYahavFile( @Nonnull Path path )
    {
        Element txTable = parseDocument( path ).body().getElementById( "mytable_body" );
        if( txTable == null )
        {
            throw new TransactionsParseException( "the BankYahav file at '" + path + "' is in unknown format (could not find table 'mytable_body')" );
        }

        AtomicInteger count = new AtomicInteger( 0 );
        txTable.getElementsByClass( "TR_ROW_BANKTABLE" )
               .stream()
               .filter( rowElement -> rowElement.getElementsByTag( "td" ).size() == 5 )
               .map( trElement -> {
                   Elements tdElements = trElement.getElementsByTag( "td" );
                   return new TxRow( null,
                                     tdElements.get( 0 ),
                                     tdElements.get( 1 ),
                                     tdElements.get( 2 ),
                                     tdElements.get( 3 ),
                                     tdElements.get( 4 ) );
               } )
               .filter( row -> {
                   Transaction duplicate = this.dao.findDuplicateTransaction( row.getCategoryId(),
                                                                              row.getReference(),
                                                                              row.getDate(),
                                                                              row.getAmount(),
                                                                              row.getComment() );
                   return duplicate == null;
               } )
               .forEach( row -> {
                   this.dao.create( row.getCategoryId(), row.getReference(), row.getDate(), row.getAmount(), row.getComment() );
                   count.incrementAndGet();
               } );
        LOG.info( "Loaded {} new transactions", count.get() );
    }

    @Nonnull
    private Document parseDocument( @Nonnull Path path )
    {
        try( InputStream inputStream = Files.newInputStream( path ) )
        {
            LOG.debug( "Reading transactions from '{}'", path );
            return Jsoup.parse( inputStream, "ISO-8859-8", "https://www.bankhapoalim.co.il" );
        }
        catch( Exception e )
        {
            throw new TransactionsParseException( "error reading or parsing file at '" + path + "'", e );
        }
    }

    @Nonnull
    private synchronized BigDecimal parseDecimal( @Nonnull String text )
    {
        try
        {
            return ( BigDecimal ) this.decimalFormat.parse( text );
        }
        catch( ParseException e )
        {
            throw new TransactionsParseException( "could not parse decimal from '" + text + "'", e );
        }
    }

    @Nullable
    private synchronized BigDecimal parseAmount( @Nullable String credit, @Nullable String charge )
    {
        try
        {
            if( charge != null )
            {
                return ( BigDecimal ) this.decimalFormat.parse( "-" + charge );
            }
            else if( credit != null )
            {
                return ( BigDecimal ) this.decimalFormat.parse( credit );
            }
            else
            {
                return null;
            }
        }
        catch( ParseException e )
        {
            throw new TransactionsParseException( "Error parsing transaction amount from charge value '" + charge + "' and credit value '" + credit + "'", e );
        }
    }

    @Nonnull
    private synchronized BigDecimal requireAmount( @Nullable String credit, @Nullable String charge )
    {
        BigDecimal amount = parseAmount( credit, charge );
        if( amount == null )
        {
            throw new TransactionsParseException( "neither credit nor charge were passed" );
        }
        else
        {
            return amount;
        }
    }

    private class TxRow
    {
        @Nullable
        private final BigDecimal balanceAfterTx;

        @Nonnull
        private final BigDecimal amount;

        @Nonnull
        private final String reference;

        @Nonnull
        private final String comment;

        @Nonnull
        private final LocalDate date;

        @Nullable
        private final Category category;

        public TxRow( @Nullable Element balanceAfterTxElement,
                      @Nonnull Element creditElement,
                      @Nonnull Element chargeElement,
                      @Nonnull Element referenceElement,
                      @Nonnull Element commentElement,
                      @Nonnull Element dateElement )
        {
            //
            // save the account balance after this tx has been executed
            //
            this.balanceAfterTx = balanceAfterTxElement == null ? null : parseDecimal( requireText( balanceAfterTxElement ) );

            //
            // calculate amount
            //
            this.amount = requireAmount( extractText( creditElement ), extractText( chargeElement ) );

            //
            // reference
            //
            this.reference = trimLeadingCharacter( requireText( referenceElement ), '0' );

            //
            // extract comment
            //
            this.comment = reverse( requireText( commentElement ) );

            //
            // calculate transaction date
            //
            TemporalAccessor accessor = dayMonthAndShortYearFormatter.parse( requireText( dateElement ) );
            this.date = LocalDate.of( accessor.get( YEAR ), accessor.get( MONTH_OF_YEAR ), accessor.get( DAY_OF_MONTH ) );

            //
            // automatically detect category
            //
            this.category = categoryServices.findAppropriateCategory( this.amount, this.reference, this.comment );
        }

        @Nonnull
        public BigDecimal getBalanceAfterTx()
        {
            return Objects.requireNonNull( this.balanceAfterTx );
        }

        @Nonnull
        public BigDecimal getAmount()
        {
            return this.amount;
        }

        @Nonnull
        public String getReference()
        {
            return this.reference;
        }

        @Nonnull
        public String getComment()
        {
            return this.comment;
        }

        @Nonnull
        public LocalDate getDate()
        {
            return this.date;
        }

        @Nullable
        public Integer getCategoryId()
        {
            return this.category == null ? null : this.category.getId();
        }
    }
}

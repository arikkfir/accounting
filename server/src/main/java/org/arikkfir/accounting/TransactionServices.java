package org.arikkfir.accounting;

import org.arikkfir.accounting.framework.persistence.ReadOnly;
import org.arikkfir.accounting.framework.persistence.ReadWrite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author arik
 */
@RestController
@RequestMapping( "/api/transactions" )
public class TransactionServices
{
    @Nonnull
    private final Dao dao;

    @Autowired
    public TransactionServices( @Nonnull Dao dao )
    {
        this.dao = dao;
    }

    @RequestMapping( method = GET )
    @ReadOnly
    public List<Tx> getAll( @Nullable
                            @RequestParam( required = false )
                            @DateTimeFormat( iso = DATE )
                            LocalDate from,

                            @Nullable
                            @RequestParam( required = false )
                            @DateTimeFormat( iso = DATE )
                            LocalDate until,

                            @Nullable
                            @RequestParam( required = false )
                            Long limit )
    {
        List<Transaction> transactions;
        if( from != null )
        {
            if( until != null )
            {
                transactions = this.dao.findBetweenDates( from, until );
            }
            else
            {
                transactions = this.dao.findFromDate( from );
            }
        }
        else if( until != null )
        {
            transactions = this.dao.findUntilDate( until );
        }
        else
        {
            transactions = this.dao.findAll();
        }
        return transactions.stream()
                           .map( Tx::new )
                           .limit( limit == null ? Long.MAX_VALUE : limit )
                           .collect( toList() );
    }

    @RequestMapping( method = GET, value = "/{id}" )
    @ReadOnly
    public ResponseEntity<?> getById( @PathVariable int id )
    {
        try
        {
            return ResponseEntity.ok( new Tx( this.dao.findById( id ) ) );
        }
        catch( EmptyResultDataAccessException e )
        {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping( method = POST )
    @ReadWrite
    public ResponseEntity<Void> create( @RequestParam
                                        int categoryId,

                                        @Nonnull
                                        @RequestParam
                                        LocalDate transactionDate,

                                        @Nonnull
                                        @RequestParam
                                        BigDecimal amount,

                                        @Nonnull
                                        @RequestParam
                                        String reference,

                                        @Nullable
                                        @RequestParam
                                        String comment )
    {
        Transaction transaction = this.dao.create( categoryId, reference, transactionDate, amount, comment );
        return ResponseEntity.created( URI.create( "/api/transactions/" + transaction.getId() ) ).build();
    }

    public static class Tx
    {
        public final int id;

        @Nonnull
        public final LocalDate date;

        @Nullable
        public final String comment;

        @Nonnull
        public final BigDecimal amount;

        @Nullable
        public final Integer categoryId;

        @Nullable
        public final String categoryName;

        @Nonnull
        public final String reference;

        private Tx( @Nonnull Transaction tx )
        {
            this.id = tx.getId();
            this.date = tx.getTransactionDate();
            this.comment = tx.getComment();
            this.amount = tx.getAmount();
            Category category = tx.getCategory();
            this.categoryId = category == null ? null : category.getId();
            this.categoryName = category == null ? null : category.getName();
            this.reference = tx.getReference();
        }
    }
}

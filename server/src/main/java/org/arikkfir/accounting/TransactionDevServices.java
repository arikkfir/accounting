package org.arikkfir.accounting;

import org.arikkfir.accounting.framework.persistence.impl.DevDatabasePopulator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Paths;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author arik
 */
@RestController
@RequestMapping( "/api/transactions" )
@Profile( "dev" )
public class TransactionDevServices
{
    @Nonnull
    private final DevDatabasePopulator devDatabasePopulator;

    @Nonnull
    private final BankTransactionsLoader transactionsLoader;

    @Autowired
    public TransactionDevServices(
            @Nonnull DevDatabasePopulator devDatabasePopulator, @Nonnull BankTransactionsLoader transactionsLoader )
    {
        this.devDatabasePopulator = devDatabasePopulator;
        this.transactionsLoader = transactionsLoader;
    }

    @RequestMapping( value = "/reload", method = POST, produces = "application/json" )
    public ResponseEntity<?> reload() throws IOException
    {
        this.devDatabasePopulator.createDatabase();
        return ResponseEntity.status( HttpStatus.OK ).contentType( APPLICATION_JSON ).body( "{}" );
    }

    @RequestMapping( value = "/load/bankhapoalim", method = POST, produces = "application/json" )
    public ResponseEntity<?> loadBankHapoalim( @RequestParam( "file" ) String pathName ) throws IOException
    {
        this.transactionsLoader.loadBankHapoalimFile( Paths.get( pathName ) );
        return ResponseEntity.status( HttpStatus.OK ).contentType( APPLICATION_JSON ).body( "{}" );
    }

    @RequestMapping( value = "/load/bankyahav", method = POST, produces = "application/json" )
    public ResponseEntity<?> loadBankYahav( @RequestParam( "file" ) String pathName ) throws IOException
    {
        this.transactionsLoader.loadBankYahavFile( Paths.get( pathName ) );
        return ResponseEntity.status( HttpStatus.OK ).contentType( APPLICATION_JSON ).body( "{}" );
    }
}

package org.arikkfir.accounting.framework.persistence.impl;

import org.arikkfir.accounting.Accounting;
import org.arikkfir.accounting.BankTransactionsLoader;
import org.arikkfir.accounting.framework.persistence.ReadWrite;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.UrlResource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Paths;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 */
@Component
@Profile( "dev" )
public class DevDatabasePopulator implements ApplicationListener<Accounting.ApplicationInitializedEvent>
{
    private static final Logger LOG = getLogger( DevDatabasePopulator.class );

    @Nonnull
    private final DataSource dataSource;

    @Nonnull
    private final DatabasePopulator databasePopulator;

    @Nonnull
    private final BankTransactionsLoader transactionsLoader;

    @Autowired
    public DevDatabasePopulator( @Nonnull DataSource dataSource, @Nonnull BankTransactionsLoader transactionsLoader )
    {
        this.dataSource = dataSource;
        ResourceDatabasePopulator resourceDatabasePopulator =
                new ResourceDatabasePopulator( new UrlResource( getClass().getResource( "/schema.sql" ) ),
                                               new UrlResource( getClass().getResource( "/data.sql" ) ) );
        resourceDatabasePopulator.setContinueOnError( false );
        resourceDatabasePopulator.setIgnoreFailedDrops( true );
        this.databasePopulator = resourceDatabasePopulator;

        this.transactionsLoader = transactionsLoader;
    }

    @ReadWrite
    public void createDatabase() throws IOException
    {
        LOG.info( "Initializing database..." );
        DatabasePopulatorUtils.execute( this.databasePopulator, this.dataSource );

        String initBankHapoalim = System.getProperty( "initBankHapoalim" );
        if( initBankHapoalim != null )
        {
            this.transactionsLoader.loadBankHapoalimFile( Paths.get( initBankHapoalim ) );
        }

        String initBankYahav = System.getProperty( "initBankYahav" );
        if( initBankYahav != null )
        {
            this.transactionsLoader.loadBankYahavFile( Paths.get( initBankYahav ) );
        }
    }

    @Override
    public void onApplicationEvent( Accounting.ApplicationInitializedEvent event )
    {
        try
        {
            createDatabase();
        }
        catch( Exception e )
        {
            LOG.error( "Could not initialize database: {}", e.getMessage(), e );
            SpringApplication.exit( event.getSource(), () -> 1 );
        }
    }
}

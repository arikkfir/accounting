package org.arikkfir.accounting.framework;

import org.slf4j.Logger;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.util.MimeType;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 * @on 1/18/15.
 */
@ControllerAdvice( annotations = RestController.class )
public class RestExceptionHandlers
{
    private static final Logger LOG = getLogger( RestExceptionHandlers.class );

    @ResponseBody
    @ExceptionHandler( value = NoSuchRequestHandlingMethodException.class )
    public ResponseEntity<?> notFound()
    {
        return ResponseEntity.notFound().build();
    }

    @ResponseBody
    @ExceptionHandler
    public ResponseEntity<?> notAllowed( @Nonnull HttpRequestMethodNotSupportedException throwable )
    {
        Set<HttpMethod> methods = throwable.getSupportedHttpMethods();
        return ResponseEntity.status( HttpStatus.METHOD_NOT_ALLOWED )
                             .header( HttpHeaders.ALLOW, methods.stream()
                                                                .map( Enum::name )
                                                                .collect( Collectors.toList() )
                                                                .toArray( new String[ methods.size() ] ) )
                             .build();
    }

    @ResponseBody
    @ExceptionHandler
    public ResponseEntity<?> unsupportedMediaType( @Nonnull HttpMediaTypeNotSupportedException throwable )
    {
        List<MediaType> methods = throwable.getSupportedMediaTypes();
        return ResponseEntity.status( HttpStatus.UNSUPPORTED_MEDIA_TYPE )
                             .header( HttpHeaders.ACCEPT, methods.stream()
                                                                 .map( MimeType::toString )
                                                                 .collect( Collectors.toList() )
                                                                 .toArray( new String[ methods.size() ] ) )
                             .build();
    }

    @ResponseBody
    @ExceptionHandler( value = HttpMediaTypeNotAcceptableException.class )
    public ResponseEntity<?> notAcceptable()
    {
        return ResponseEntity.status( HttpStatus.NOT_ACCEPTABLE ).build();
    }

    @ResponseBody
    @ExceptionHandler( value = MissingServletRequestParameterException.class )
    public ResponseEntity<?> missingServletRequestParameter( @Nonnull MissingServletRequestParameterException exception,
                                                             @Nonnull HttpServletRequest request )
    {
        LOG.warn( "Bad request: {}", exception.getMessage(), exception );
        return ResponseEntity.badRequest().build();
    }

    @ResponseBody
    @ExceptionHandler( value = ServletRequestBindingException.class )
    public ResponseEntity<?> servletRequestBindingError()
    {
        return ResponseEntity.badRequest().build();
    }

    @ResponseBody
    @ExceptionHandler( value = { ConversionNotSupportedException.class } )
    public ResponseEntity<?> conversionNotSupported()
    {
        return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR ).build();
    }

    @ResponseBody
    @ExceptionHandler( value = TypeMismatchException.class )
    public ResponseEntity<?> typeMismatch()
    {
        return ResponseEntity.badRequest().build();
    }

    @ResponseBody
    @ExceptionHandler( value = { HttpMessageNotReadableException.class } )
    public ResponseEntity<?> messageNotReadable()
    {
        return ResponseEntity.badRequest().build();
    }

    @ResponseBody
    @ExceptionHandler
    public ResponseEntity<?> messageNotWritable( @Nonnull HttpMessageNotWritableException exception )
    {
        LOG.error( "Could not write message: {}", exception.getMessage(), exception );
        return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR ).build();
    }

    @ResponseBody
    @ExceptionHandler( value = { MethodArgumentNotValidException.class } )
    public ResponseEntity<?> argumentNotValid()
    {
        return ResponseEntity.badRequest().build();
    }

    @ResponseBody
    @ExceptionHandler( value = { MissingServletRequestPartException.class } )
    public ResponseEntity<?> missingRequestPart()
    {
        return ResponseEntity.badRequest().build();
    }

    @ResponseBody
    @ExceptionHandler( value = { BindException.class } )
    public ResponseEntity<?> bindException()
    {
        return ResponseEntity.badRequest().build();
    }

    @ResponseBody
    @ExceptionHandler( value = { NoHandlerFoundException.class } )
    public ResponseEntity<?> noHandlerFound()
    {
        return ResponseEntity.badRequest().build();
    }

    @ResponseBody
    @ExceptionHandler
    public ResponseEntity<?> error( @Nonnull HttpServletRequest request, @Nonnull Throwable throwable )
    {
        LOG.warn( "Internal error while processing '{}': {}", request.getRequestURL(), throwable.getMessage(), throwable );
        return ResponseEntity.status( HttpStatus.INTERNAL_SERVER_ERROR ).build();
    }
}

package org.arikkfir.accounting.framework;

import java.util.UUID;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * @author arik
 */
@Component
public class UuidInterceptor extends HandlerInterceptorAdapter
{
    private static final String UUID_KEY = UuidInterceptor.class.getName() + "#" + UUID.class.getName();

    @Nonnull
    private static final ThreadLocal<UUID> uuidHolder = new ThreadLocal<>();

    @Override
    public boolean preHandle( @Nonnull HttpServletRequest request,
                              @Nonnull HttpServletResponse response,
                              @Nonnull Object handler )
            throws Exception
    {
        Object uuid = request.getAttribute( UUID_KEY );
        if( uuid == null )
        {
            String header = request.getHeader( "X-Uuid" );
            if( header == null )
            {
                uuid = UUID.randomUUID();
            }
            else
            {
                uuid = UUID.fromString( header );
            }
            request.setAttribute( UUID_KEY, uuid );
            uuidHolder.set( ( UUID ) uuid );
        }
        response.setHeader( "X-Uuid", uuid.toString() );
        return true;
    }

    @Override
    public void afterCompletion( @Nonnull HttpServletRequest request,
                                 @Nonnull HttpServletResponse response,
                                 @Nonnull Object handler,
                                 @Nullable Exception ex ) throws Exception
    {
        request.removeAttribute( UUID_KEY );
        uuidHolder.remove();
    }
}

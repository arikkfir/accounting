package org.arikkfir.accounting.framework.persistence.impl;

import org.arikkfir.accounting.Dao;
import org.arikkfir.dao.DaoFactory;
import org.arikkfir.dao.impl.DaoFactoryImpl;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Objects;

/**
 * @author arik
 */
@Configuration
@ConfigurationProperties( prefix = "datasource" )
public class DatabaseBeans
{
    private String url;

    private String username;

    private String password;

    @Bean
    public DataSource dataSource()
    {
        // TODO: set-up connection pooling
        return new DriverManagerDataSource( Objects.requireNonNull( this.url, "JDBC URL not configured" ), this.username, this.password );
    }

    @Bean
    public PlatformTransactionManager transactionManager()
    {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager( dataSource() );
        transactionManager.setDefaultTimeout( 120 );
        transactionManager.setFailEarlyOnGlobalRollbackOnly( true );
        transactionManager.setGlobalRollbackOnParticipationFailure( true );
        transactionManager.setRollbackOnCommitFailure( true );
        transactionManager.setValidateExistingTransaction( true );
        return transactionManager;
    }

    @Bean
    public DaoFactory daoFactory()
    {
        return new DaoFactoryImpl( jdbcTemplate(), namedParameterJdbcTemplate() );
    }

    @Bean
    public Dao dao()
    {
        return daoFactory().create( Dao.class );
    }

    @Bean
    public JdbcTemplate jdbcTemplate()
    {
        return new JdbcTemplate( dataSource() );
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate()
    {
        return new NamedParameterJdbcTemplate( jdbcTemplate() );
    }

    /**
     * Called by Spring Boot's configuration mechanism.
     */
    @SuppressWarnings( "UnusedDeclaration" )
    public void setUrl( String url )
    {
        this.url = url;
    }

    /**
     * Called by Spring Boot's configuration mechanism.
     */
    @SuppressWarnings( "UnusedDeclaration" )
    public void setUsername( String username )
    {
        this.username = username;
    }

    /**
     * Called by Spring Boot's configuration mechanism.
     */
    @SuppressWarnings( "UnusedDeclaration" )
    public void setPassword( String password )
    {
        this.password = password;
    }
}

package org.arikkfir.accounting.framework;

import java.time.Instant;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Role;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import static java.io.File.pathSeparator;
import static java.lang.System.getProperty;
import static java.lang.management.ManagementFactory.getOperatingSystemMXBean;
import static java.lang.management.ManagementFactory.getRuntimeMXBean;
import static org.slf4j.LoggerFactory.getLogger;

/**
 * @author arik
 */
@Component
@Role( BeanDefinition.ROLE_INFRASTRUCTURE )
public class SystemInfo
{
    private static final Logger LOG = getLogger( SystemInfo.class );

    private static final String[] NO_TOKENS = new String[] { "None" };

    private static final Set<String> EXCLUDED_SYSTEM_PROPS = new HashSet<String>()
    {
        {
            add( "java.class.path" );
            add( "java.class.version" );
            add( "java.endorsed.dirs" );
            add( "java.ext.dirs" );
            add( "java.home" );
            add( "java.io.tmpdir" );
            add( "java.library.path" );
            add( "java.specification.name" );
            add( "java.specification.vendor" );
            add( "java.specification.version" );
            add( "java.runtime.name" );
            add( "java.runtime.version" );
            add( "java.vendor" );
            add( "java.vendor.url" );
            add( "java.version" );
            add( "java.vm.name" );
            add( "java.vm.specification.name" );
            add( "java.vm.specification.vendor" );
            add( "java.vm.specification.version" );
            add( "java.vm.vendor" );
            add( "java.vm.version" );
            add( "line.separator" );
            add( "os.arch" );
            add( "os.name" );
            add( "os.version" );
            add( "sun.arch.data.model" );
            add( "sun.boot.class.path" );
            add( "sun.boot.library.path" );
            add( "sun.java.command" );
            add( "user.country" );
            add( "user.dir" );
            add( "user.home" );
            add( "user.language" );
            add( "user.name" );
            add( "user.timezone" );
        }
    };

    @Value( "${git.commit.id.abbrev}" )
    private String abbrevCommitId;

    @Value( "${git.commit.id}" )
    private String commitId;

    @Value( "${git.branch}" )
    private String branch;

    @Value( "${git.build.time}" )
    private String buildTime;

    @Value( "${project.version}" )
    private String version;

    @Autowired
    private Environment environment;

    @PostConstruct
    public void init()
    {
        if( this.environment.acceptsProfiles( "sysInfo", "!dev" ) )
        {
            printSystemInfo();
        }
    }

    private void printSystemInfo()
    {
        LOG.info( "**************************************************************************************************" );
        LOG.info( "" );
        LOG.info( "___APPLICATION INFO_______________________________________________________________________________" );
        LOG.info( "               Build time : {}", this.buildTime );
        LOG.info( "                 GIT info : {} ({} / {})", this.branch, this.abbrevCommitId, this.commitId );
        LOG.info( "                  Version : {}", this.version );
        LOG.info( "" );
        LOG.info( "___USER INFO______________________________________________________________________________________" );
        LOG.info( "                     Name : {}", getProperty( "user.name" ) );
        LOG.info( "                     Home : {}", getProperty( "user.home" ) );
        LOG.info( "                 Language : {}", getProperty( "user.language" ) );
        LOG.info( "                  Country : {}", getProperty( "user.country" ) );
        LOG.info( "                Time-zone : {}", getProperty( "user.timezone" ) );
        LOG.info( "" );
        LOG.info( "___PROCESS INFO___________________________________________________________________________________" );
        LOG.info( "                  Process : {}", getRuntimeMXBean().getName() );
        LOG.info( "               Start time : {}", Instant.ofEpochMilli( getRuntimeMXBean().getStartTime() ).atZone( ZoneId.systemDefault() ) );
        LOG.info( "        Working directory : {}", getProperty( "user.dir" ) );
        LOG.info( "      Temporary directory : {}", getProperty( "java.io.tmpdir" ) );
        printDelimitedPaths( "Arguments", getProperty( "sun.java.command" ), " " );
        LOG.info( "" );
        LOG.info( "___SYSTEM INFO____________________________________________________________________________________" );
        LOG.info( "     Available processors : {}", getOperatingSystemMXBean().getAvailableProcessors() );
        LOG.info( "         Operating system : {} ({}, {}bit) {}", getProperty( "os.name" ), getProperty( "os.arch" ), getProperty( "sun.arch.data.model" ), getProperty( "os.version" ) );
        LOG.info( "" );
        LOG.info( "___JAVA VIRTUAL MACHINE INFO______________________________________________________________________" );
        LOG.info( "                Java home : {}", getProperty( "java.home" ) );
        LOG.info( "                  Version : {} ({}, {})", getProperty( "java.version" ), getProperty( "java.vendor" ), getProperty( "java.vendor.url" ) );
        LOG.info( "            Specification : {} {} ({})", getProperty( "java.specification.name" ), getProperty( "java.specification.version" ), getProperty( "java.specification.vendor" ) );
        LOG.info( "                            {} {} ({})", getProperty( "java.vm.specification.name" ), getProperty( "java.vm.specification.version" ), getProperty( "java.vm.specification.vendor" ) );
        LOG.info( "                  Runtime : {} {}", getProperty( "java.runtime.name" ), getProperty( "java.runtime.version" ) );
        LOG.info( "                            {} {} ({})", getProperty( "java.vm.name" ), getProperty( "java.vm.version" ), getProperty( "java.vm.vendor" ) );
        LOG.info( "     Class format version h: {}", getProperty( "java.class.version" ) );
        LOG.info( "" );
        LOG.info( "___BOOT LIBRARY & CLASS PATHS_____________________________________________________________________" );
        printDelimitedPaths( "Boot class-path", getProperty( "sun.boot.class.path" ) );
        LOG.info( "" );
        printDelimitedPaths( "Boot library-path", getProperty( "sun.boot.library.path" ) );
        LOG.info( "" );
        LOG.info( "___LIBRARY & CLASS PATHS__________________________________________________________________________" );
        printDelimitedPaths( "Class-path", getProperty( "java.class.path" ) );
        LOG.info( "" );
        printDelimitedPaths( "Library-path", getProperty( "java.library.path" ) );
        LOG.info( "" );
        printDelimitedPaths( "Endorsed standards", getProperty( "java.endorsed.dirs" ) );
        LOG.info( "" );
        printDelimitedPaths( "Extensions directories", getProperty( "java.ext.dirs" ) );
        LOG.info( "" );
        LOG.info( "___SYSTEM PROPERTIES______________________________________________________________________________" );
        new TreeMap<>( getRuntimeMXBean().getSystemProperties() )
                .entrySet()
                .stream()
                .filter( entry -> !EXCLUDED_SYSTEM_PROPS.contains( entry.getKey() ) )
                .forEach( entry -> LOG.info( padFromLeft( entry.getKey(), 35, ' ' ) + " : {}", entry.getValue() ) );

        LOG.info( "" );
        LOG.info( "**************************************************************************************************" );
    }

    private void printDelimitedPaths( @Nonnull String label, @Nullable String values )
    {
        printDelimitedPaths( label, values, pathSeparator );
    }

    private void printDelimitedPaths( @Nonnull String label, @Nullable String values, @Nonnull String delim )
    {
        String[] tokens = values != null ? values.split( Pattern.quote( delim ) ) : NO_TOKENS;
        printDelimitedPaths( label, tokens );
    }

    private void printDelimitedPaths( @Nonnull String label, @Nullable Collection<String> values )
    {
        printDelimitedPaths( label, values == null ? null : values.toArray( new String[ values.size() ] ) );
    }

    private void printDelimitedPaths( @Nonnull String label, @Nullable String[] values )
    {
        String[] tokens = values != null ? values : NO_TOKENS;

        StringBuilder noLabelBuf = new StringBuilder( padFromLeft( "", 28, ' ' ) ).append( "{}" );

        Iterator<String> iterator = Stream.of( tokens ).iterator();
        LOG.info( padFromLeft( label, 25, ' ' ) + " : {}", iterator.next() );
        while( iterator.hasNext() )
        {
            String next = iterator.next();
            if( next != null && !next.isEmpty() )
            {
                LOG.info( noLabelBuf.toString(), next );
            }
        }
    }

    @Nonnull
    private String padFromLeft( @Nonnull String string, int size, char c )
    {
        StringBuilder buffer = new StringBuilder( size );
        for( int i = 0; i < size - string.length(); i++ )
        {
            buffer.append( c );
        }
        buffer.append( string );
        return buffer.toString();
    }
}

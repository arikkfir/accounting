package org.arikkfir.accounting.framework.persistence.impl;

import org.arikkfir.accounting.Accounting;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import javax.annotation.Nonnull;

/**
 * @author arik
 */
@Aspect
public class TransactionAdvice
{
    @Around( "execution( @org.arikkfir.accounting.framework.persistence.ReadOnly * *.*(..) )" )
    public Object executeInReadOnlyTx( @Nonnull ProceedingJoinPoint joinPoint ) throws Throwable
    {
        return execute( joinPoint, true );
    }

    @Around( "execution( @org.arikkfir.accounting.framework.persistence.ReadWrite * *.*(..) )" )
    public Object executeInReadWriteTx( @Nonnull ProceedingJoinPoint joinPoint ) throws Throwable
    {
        return execute( joinPoint, false );
    }

    private Object execute( @Nonnull ProceedingJoinPoint joinPoint, boolean readOnly ) throws Throwable
    {
        // create transaction definition
        DefaultTransactionDefinition tx = new DefaultTransactionDefinition();
        tx.setName( joinPoint.toLongString() );
        tx.setReadOnly( readOnly );

        // create transaction
        PlatformTransactionManager txMgr = Accounting.getApplication().getTransactionManager();
        TransactionStatus status = txMgr.getTransaction( tx );

        // execute!
        try
        {
            Object result = joinPoint.proceed();
            txMgr.commit( status );
            return result;
        }
        catch( Throwable applicationEx )
        {
            // Transactional code threw application exception -> rollback
            txMgr.rollback( status );
            throw applicationEx;
        }
    }
}

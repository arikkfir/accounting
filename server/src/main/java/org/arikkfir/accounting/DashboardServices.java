package org.arikkfir.accounting;

import org.arikkfir.accounting.Dao.MonthCategoryAmount;
import org.arikkfir.accounting.framework.persistence.ReadOnly;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.*;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author arik
 */
@RestController
@RequestMapping( "/api/account" )
public class DashboardServices
{
    @Nonnull
    private final Dao dao;

    @Autowired
    public DashboardServices( @Nonnull Dao dao )
    {
        this.dao = dao;
    }

    @RequestMapping( value = "/categories/expenses", method = GET )
    @ReadOnly
    public Map<YearMonth, Map<String, BigDecimal>> getExpensesByCategoryOverTime( @Nonnull
                                                                                  @DateTimeFormat( pattern = "yyyy-M-d" )
                                                                                  @RequestParam
                                                                                  LocalDate date )
    {
        List<MonthCategoryAmount> amounts = this.dao.selectExpensesAndIncomeByCategories( LocalDate.of( 1970, 1, 1 ), date, BigDecimal.ZERO );
        return aggregateCategorizedTransactionsByMonth(
                amounts.stream().filter( row -> row.getAmount().compareTo( BigDecimal.ZERO ) < 0 ) );
    }

    @RequestMapping( value = "/categories/income", method = GET )
    @ReadOnly
    public Map<YearMonth, Map<String, BigDecimal>> getIncomeByCategoryOverTime( @Nonnull
                                                                                @DateTimeFormat( pattern = "yyyy-M-d" )
                                                                                @RequestParam
                                                                                LocalDate date )
    {
        List<MonthCategoryAmount> amounts = this.dao.selectExpensesAndIncomeByCategories( LocalDate.of( 1970, 1, 1 ), date, BigDecimal.ZERO );
        return aggregateCategorizedTransactionsByMonth(
                amounts.stream().filter( row -> row.getAmount().compareTo( BigDecimal.ZERO ) > 0 ) );
    }

    @Nonnull
    private Map<YearMonth, Map<String, BigDecimal>> aggregateCategorizedTransactionsByMonth(
            @Nonnull Stream<MonthCategoryAmount> amounts )
    {
        Map<YearMonth, List<MonthCategoryAmount>> data =
                new TreeMap<>( amounts.collect( groupingBy( MonthCategoryAmount::getYearAndMonth ) ) );

        // fetch all known categories so we can fill categories that don't appear in certain months with zeros
        List<Category> categories = this.dao.findAllCategories();

        // convert data to a map of yearMonth->Map[CategoryName->Amount]
        Map<YearMonth, Map<String, BigDecimal>> mappedData = new TreeMap<>();
        for( Map.Entry<YearMonth, List<MonthCategoryAmount>> entry : data.entrySet() )
        {
            // the value is a list of rows (each row has category and amount); convert the rows into a map of category->amount
            Map<String, BigDecimal> categoryAmounts =
                    new TreeMap<>(
                            entry.getValue()
                                 .stream()
                                 .collect( groupingBy( MonthCategoryAmount::getCategoryName,
                                                       mapping( MonthCategoryAmount::getAmount,
                                                                reducing( BigDecimal.ZERO, BigDecimal::add ) ) ) ) );

            // fill-in missing categories for this month with zeros
            for( Category category : categories )
            {
                categoryAmounts.computeIfAbsent( category.getName(), name -> BigDecimal.ZERO );
            }
            mappedData.put( entry.getKey(), categoryAmounts );
        }
        return mappedData;
    }

    @RequestMapping( method = GET )
    @ReadOnly
    public AccountStatus getBalanceOverTime( @Nonnull
                                             @DateTimeFormat( pattern = "yyyy-M-d" )
                                             @RequestParam
                                             LocalDate date )
    {
        //
        // get expenses & income for this month
        //
        LocalDate thisMonthStart = LocalDate.of( date.getYear(), date.getMonth(), 1 );
        LocalDate thisMonthEnd = thisMonthStart.plusMonths( 1 ).minusDays( 1 );
        Collection<Transaction> thisMonthTransactions = this.dao.findBetweenDates( thisMonthStart, thisMonthEnd );
        BigDecimal thisMonthExpenses = new BigDecimal( thisMonthTransactions.stream()
                                                                            .filter( tx -> tx.getAmount().compareTo( BigDecimal.ZERO ) < 0 )
                                                                            .mapToDouble( value -> value.getAmount().abs().doubleValue() )
                                                                            .sum(), MathContext.UNLIMITED );
        BigDecimal thisMonthIncome = new BigDecimal( thisMonthTransactions.stream()
                                                                          .filter( tx -> tx.getAmount().compareTo( BigDecimal.ZERO ) > 0 )
                                                                          .mapToDouble( value -> value.getAmount().doubleValue() )
                                                                          .sum(), MathContext.UNLIMITED );

        //
        // get expenses & income for the previous month
        //
        LocalDate prevMonthStart = thisMonthStart.minusMonths( 1 );
        LocalDate prevMonthEnd = prevMonthStart.plusMonths( 1 ).minusDays( 1 );
        Collection<Transaction> prevMonthTransactions = this.dao.findBetweenDates( prevMonthStart, prevMonthEnd );
        BigDecimal prevMonthExpenses = new BigDecimal( prevMonthTransactions.stream()
                                                                            .filter( tx -> tx.getAmount().compareTo( BigDecimal.ZERO ) < 0 )
                                                                            .mapToDouble( value -> value.getAmount().abs().doubleValue() )
                                                                            .sum(), MathContext.UNLIMITED );
        BigDecimal prevMonthIncome = new BigDecimal( prevMonthTransactions.stream()
                                                                          .filter( tx -> tx.getAmount().compareTo( BigDecimal.ZERO ) > 0 )
                                                                          .mapToDouble( value -> value.getAmount().doubleValue() )
                                                                          .sum(), MathContext.UNLIMITED );

        //
        // get balance-over-time up to selected date
        //
        ConcurrentNavigableMap<LocalDate, BigDecimal> balanceOverTime = new ConcurrentSkipListMap<>();
        this.dao.findAll()
                .stream()
                .sorted( ( tx1, tx2 ) -> tx1.getTransactionDate().compareTo( tx2.getTransactionDate() ) )
                .forEachOrdered( tx -> {
                    LocalDate transactionDate = tx.getTransactionDate();
                    balanceOverTime.compute(
                            transactionDate,
                            ( balanceDate, value ) -> {
                                Map.Entry<LocalDate, BigDecimal> lastBalanceEntry = balanceOverTime.floorEntry( transactionDate );
                                BigDecimal amount = tx.getAmount();
                                if( value == null )
                                {
                                    return lastBalanceEntry == null ? amount : lastBalanceEntry.getValue().add( amount );
                                }
                                else
                                {
                                    return value.add( amount );
                                }
                            } );
                } );

        //
        // map categories
        //
        @SuppressWarnings( "MismatchedQueryAndUpdateOfCollection" ) Map<String, BigDecimal> thisMonthCategorySpend = new HashMap<>();
        thisMonthTransactions.stream()
                             .filter( tx -> tx.getCategory() != null )
                             .forEach( tx -> thisMonthCategorySpend.merge( tx.getCategory().getName(), tx.getAmount().abs(), BigDecimal::add ) );
        @SuppressWarnings( "MismatchedQueryAndUpdateOfCollection" ) Map<String, BigDecimal> prevMonthCategorySpend = new HashMap<>();
        prevMonthTransactions.stream()
                             .filter( tx -> tx.getCategory() != null )
                             .forEach( tx -> prevMonthCategorySpend.merge( tx.getCategory().getName(), tx.getAmount().abs(), BigDecimal::add ) );
        Map<String, CategoryStatus> categorySpend = new TreeMap<>();
        Stream.concat( thisMonthCategorySpend.keySet().stream(), prevMonthCategorySpend.keySet().stream() )
              .forEach( name -> {
                  BigDecimal thisMonthAmount = thisMonthCategorySpend.get( name );
                  BigDecimal prevMonthAmount = prevMonthCategorySpend.get( name );
                  categorySpend.put( name, new CategoryStatus( name, thisMonthAmount, prevMonthAmount ) );
              } );

        //
        // results
        //
        return new AccountStatus( date,
                                  this.dao.findBalanceForDate( date ),
                                  thisMonthExpenses,
                                  thisMonthIncome,
                                  prevMonthExpenses,
                                  prevMonthIncome,
                                  balanceOverTime,
                                  categorySpend.values() );
    }

    public static class CategoryStatus
    {
        @Nonnull
        public final String name;

        @Nonnull
        public final BigDecimal thisMonthAmount;

        @Nonnull
        public final BigDecimal prevMonthAmount;

        public CategoryStatus( @Nonnull String name,
                               @Nullable BigDecimal thisMonthAmount,
                               @Nullable BigDecimal prevMonthAmount )
        {
            this.name = name;
            this.thisMonthAmount = thisMonthAmount == null ? BigDecimal.ZERO : thisMonthAmount;
            this.prevMonthAmount = prevMonthAmount == null ? BigDecimal.ZERO : prevMonthAmount;
        }
    }

    public static class AccountStatus
    {
        @Nonnull
        public final LocalDate date;

        @Nonnull
        public final BigDecimal balanceForDate;

        @Nonnull
        public final BigDecimal thisMonthExpenses;

        @Nonnull
        public final BigDecimal thisMonthIncome;

        @Nonnull
        public final BigDecimal previousMonthExpenses;

        @Nonnull
        public final BigDecimal previousMonthIncome;

        @Nonnull
        public final ConcurrentNavigableMap<LocalDate, BigDecimal> balanceOverTime;

        @Nonnull
        public final Collection<CategoryStatus> categorySpend;

        AccountStatus( @Nonnull LocalDate date,
                       @Nonnull BigDecimal balanceForDate,
                       @Nonnull BigDecimal thisMonthExpenses,
                       @Nonnull BigDecimal thisMonthIncome,
                       @Nonnull BigDecimal previousMonthExpenses,
                       @Nonnull BigDecimal previousMonthIncome,
                       @Nonnull ConcurrentNavigableMap<LocalDate, BigDecimal> balanceOverTime,
                       @Nonnull Collection<CategoryStatus> categorySpend )
        {
            this.date = date;
            this.balanceForDate = balanceForDate;
            this.thisMonthExpenses = thisMonthExpenses;
            this.thisMonthIncome = thisMonthIncome;
            this.previousMonthExpenses = previousMonthExpenses;
            this.previousMonthIncome = previousMonthIncome;
            this.balanceOverTime = balanceOverTime;
            this.categorySpend = categorySpend;
        }
    }
}

package org.arikkfir.accounting;

import java.math.BigDecimal;
import org.arikkfir.dao.Column;
import org.arikkfir.dao.Id;
import org.arikkfir.dao.Table;

/**
 * @author arik
 */
@Table( "category_mappings" )
public interface CategoryMapping
{
    @Id
    @Column( "id" )
    int getId();

    @Column( "category_id" )
    Category getCategory();

    @Column( "comment_pattern" )
    String getCommentPattern();

    @Column( "reference_pattern" )
    String getReferencePattern();

    @Column( "min_amount" )
    BigDecimal getMinAmount();

    @Column( "max_amount" )
    BigDecimal getMaxAmount();
}

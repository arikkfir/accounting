package org.arikkfir.accounting;

/**
 * @author arik
 */
public class TransactionsParseException extends RuntimeException
{
    public TransactionsParseException( String message )
    {
        super( message );
    }

    public TransactionsParseException( String message, Throwable cause )
    {
        super( message, cause );
    }
}

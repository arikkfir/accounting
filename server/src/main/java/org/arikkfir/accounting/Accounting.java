package org.arikkfir.accounting;

import org.arikkfir.accounting.framework.UuidInterceptor;
import org.arikkfir.accounting.framework.persistence.impl.DatabaseBeans;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.Objects.requireNonNull;
import static java.util.concurrent.Executors.newScheduledThreadPool;

/**
 * The {@link Configuration @Configuration} root of the accounting application.
 *
 * @author arik
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableConfigurationProperties
@Import( DatabaseBeans.class )
@PropertySource( value = { "classpath:/git.properties", "classpath:/maven.properties" }, ignoreResourceNotFound = true )
public class Accounting
{
    //------------------------------------------------------------------------------------------------------------------
    // STATIC SECTION
    //------------------------------------------------------------------------------------------------------------------

    /**
     * The singleton reference for the application. Used by @AspectJ advices that can't access the application context.
     */
    @Nullable
    private static Accounting application;

    /**
     * The application context.
     */
    private ApplicationContext applicationContext;

    /**
     * Gets the application instance.
     *
     * @return application instance
     */
    @Nonnull
    public static Accounting getApplication()
    {
        return Objects.requireNonNull( application );
    }

    //------------------------------------------------------------------------------------------------------------------
    // STATIC CLASSES SECTION
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Set the application singleton instance. In general, in Spring, we shouldn't need this; however, since we're using
     * AspectJ "raw" (NOT Spring AOP) the aspects cannot access Spring beans directly. These aspects will therefor use
     * the global singleton application instance (which is set using this method).
     *
     * @param application the application instance
     */
    static void setApplication( @Nullable Accounting application )
    {
        Accounting.application = application;
    }

    void setApplicationContext( ApplicationContext applicationContext )
    {
        this.applicationContext = applicationContext;
    }

    /**
     * Provides the accounting application instance as a {@link Bean @Bean}.
     *
     * @return application instance
     */
    @Bean
    public Accounting accounting()
    {
        return this;
    }

    //------------------------------------------------------------------------------------------------------------------
    // INSTANCE SECTION
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Defines a {@link org.springframework.core.convert.ConversionService} for the application.
     *
     * @return conversion service
     */
    @Bean
    public ConversionServiceFactoryBean conversionService()
    {
        return new ConversionServiceFactoryBean();
    }

    /**
     * Gets the {@link PlatformTransactionManager transaction manager} for the application.
     *
     * @return transaction manager
     */
    @Nonnull
    public PlatformTransactionManager getTransactionManager()
    {
        return requireNonNull( this.applicationContext, "Application context not initialized" ).getBean( PlatformTransactionManager.class );
    }

    /**
     * Spring event class used to signal that the application really is initialized. This is different from the standard
     * Spring {@link org.springframework.context.event.ContextRefreshedEvent} and it's siblings because it is only fired
     * after the global singleton instance has been set using {@link #setApplication(Accounting)}.
     */
    public static class ApplicationInitializedEvent extends ApplicationEvent
    {
        public ApplicationInitializedEvent( @Nonnull ApplicationContext source )
        {
            super( source );
        }

        @Override
        public ApplicationContext getSource()
        {
            return ( ApplicationContext ) super.getSource();
        }
    }

    /**
     * Configures web support for the application.
     */
    @Configuration
    public static class Web extends WebMvcConfigurerAdapter
    {
        @Autowired
        private UuidInterceptor uuidInterceptor;

        @Override
        public void addInterceptors( InterceptorRegistry registry )
        {
            registry.addInterceptor( this.uuidInterceptor );
        }
    }

    /**
     * Configures scheduling support for the application.
     */
    @Configuration
    @EnableScheduling
    public static class SchedulingBeans implements SchedulingConfigurer
    {
        @Override
        public void configureTasks( @Nonnull ScheduledTaskRegistrar taskRegistrar )
        {
            taskRegistrar.setScheduler( taskExecutor() );
        }

        @Bean( destroyMethod = "shutdown" )
        public ScheduledExecutorService taskExecutor()
        {
            return newScheduledThreadPool( 100 );
        }
    }
}

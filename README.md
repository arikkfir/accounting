# What is this repository for? #

A simple home accounting application I've written to help me in managing my home expenses and income. I'm building &
maintaining this application also for R&D of best practices of how I want to develop applications.

### Requirements ###

* Maven 3.2.x
* Java 8

### Initial set-up: ###

* Checkout the code
* Run "mvn clean package"
* That's it!

### IntelliJ project set-up: ###

* Use IntelliJ's standard "Import project" facility to import the Maven project

### IntelliJ project run configuration: ###

* Create a standard "Application" run configuration (ie. "main" method)
* Choose "org.arikkfir.accounting.Accounting" as the main class
* Set the run configuration working directory to the "home" directory, or any other name (create it if needed). Can be anywhere.
* Since you'll probably want to re-create the database on every run, add "--profiles=dev" to the program arguments (NOT
  the "VM options" field)
* Add the following to the "VM options" field: -javaagent:&lt;home&gt;/.m2/repository/org/aspectj/aspectjweaver/1.8.4/aspectjweaver-1.8.4.jar
  (replace "&lt;home&gt;" with your home directory...)

### Application configuration ###

Accounting is built on Spring Boot, which means that default configuration can be found at the "accounting.properties"
and "accounting-<profile>.properties" files. These files should provide default standard configuration. For deployment
configuration, Spring Boot allows providing out-of-package configuration as well.

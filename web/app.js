var express = require( 'express' );
var path = require( 'path' );
var fs = require( 'fs' );
var favicon = require( 'serve-favicon' );
var logger = require( 'morgan' );
var cookieParser = require( 'cookie-parser' );
var bodyParser = require( 'body-parser' );
var proxy = require( 'express-http-proxy' );
var sass = require( 'node-sass' );

var app = express();
app.use( favicon( __dirname + '/public/favicon.png' ) );
app.use( logger( 'dev' ) );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( { extended: false } ) );
app.use( cookieParser() );

// compile App.css from App.scss
app.get( '*.css', function( req, res, next ) {
    var compile = function( req, res, source, generated ) {
        sass.render( {
            file: source,
            indentedSyntax: true,
            omitSourceMapUrl: app.get( 'env' ) !== 'development',
            sourceComments: app.get( 'env' ) === 'development',
            sourceMap: app.get( 'env' ) === 'development' ? generated + ".map" : null
        }, function( err, result ) {
            if( err ) {
                console.error( "error generating CSS from SASS file at '", source, "': ", err );
                res.sendStatus( 500 );
            } else {

                fs.writeFile( generated, result.css, function( err ) {
                    if( err ) {
                        console.error( "error generating CSS from SASS file at '", source, "': ", err );
                        res.sendStatus( 500 );
                    } else {
                        fs.writeFile( generated + ".map", result.map, function( err ) {
                            if( err ) {
                                console.error( "error generating CSS from SASS file at '", source, "': ", err );
                                res.sendStatus( 500 );
                            } else {
                                res.sendFile( generated, {
                                    maxAge: app.get( 'env' ) === 'development' ? 0 : 1000 * 60 * 60,
                                    headers: {
                                        "ContentType": "text/css"
                                    }
                                } );
                            }
                        } );
                    }
                } );
            }
        } );
    };

    var generated = path.join( __dirname, '/public', req.url );
    var source = generated.replace( '.css', '.scss' );
    fs.exists( source, function( sourceExists ) {

        if( !sourceExists ) {
            // no SCSS file - this is not a CSS file that's generated from a SASS (SCSS) file; ignore and move on
            next();
            return;
        }

        // SCSS file exists - check if its [cached] generated CSS exists also
        fs.exists( generated, function( generatedExists ) {
            if( generatedExists ) {

                // a generated CSS exists - compare modTime of generated vs. source
                fs.stat( source, function( sourceErr, sourceStats ) {

                    if( sourceErr ) {
                        console.error( "error generating CSS from SASS file at '", source, "': ", sourceErr );
                        res.sendStatus( 500 );
                        return;
                    }

                    fs.stat( generated, function( generatedErr, generatedStats ) {

                        if( generatedErr ) {
                            console.error( "error generating CSS from SASS file at '", source, "': ", generatedErr );
                            res.sendStatus( 500 );
                            return;
                        }

                        if( sourceStats.mtime < generatedStats.mtime ) {

                            // generated is up-to-date, no need to recompile
                            next();

                        } else {

                            // source has been updated - recompile
                            compile( req, res, source, generated );

                        }
                    } );
                } );

            } else {

                // a generated CSS does NOT exist - compile now
                compile( req, res, source, generated );

            }
        } );
    } );
} );

// proxy "/api/..." requests to services server
app.use( '/api/', proxy( 'localhost:8080', {
    forwardPath: function( req ) {
        return req.originalUrl;
    }
} ) );

// serve static resources from 'public'
app.use( express.static( path.join( __dirname, 'public' ) ) );

// catch 404 and forward to error handler
app.use( function( req, res, next ) {
    var err = new Error( 'Not Found' );
    err.status = 404;
    next( err );
} );

// development error handler which will print stacktrace
if( app.get( 'env' ) === 'development' ) {
    app.use( function( err, req, res ) {
        res.status( err.status || 500 );
        res.render( 'error', {
            message: err.message,
            error: err
        } );
    } );
}

// production error handler, no stacktraces leaked to user
app.use( function( err, req, res ) {
    res.status( err.status || 500 );
    res.render( 'error', {
        message: err.message,
        error: {}
    } );
} );

module.exports = app;

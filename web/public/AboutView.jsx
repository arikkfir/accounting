app.AboutView = React.createClass( {
    mixins: [ app.Page ],
    getTitle: function() {
        return 'אודות';
    },
    renderContents: function() {
        return (
            <p>
                מערכת זו נכתבה ע"י אריק כפיר, על מנת לעזור בניהול חשבונות הבית, חשבון הבנק, וכלל הפעולות הפיננסיות
                שלנו.
            </p>
        );
    }
} );

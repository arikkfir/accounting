app.AppView = React.createClass( {
    displayName: "App",
    mixins: [ ReactRouter.State ],
    render: function() {
        var brand =
            <a className="navbar-brand" href="#">
                <img src="/cash.png"/>
                &nbsp;
                ניהול חשבון
            </a>;

        return (
            <div>
                <header id="pageHeader">
                    <ReactBootstrap.Navbar inverse={true} fixedTop={true} fluid={true} brand={brand}/>
                </header>
                <section id="sidebar">
                    <ReactBootstrap.Nav bsStyle="pills" stacked={true}>
                        <ReactRouterBootstrap.NavItemLink to="dashboard">מבט על</ReactRouterBootstrap.NavItemLink>
                        <ReactRouterBootstrap.NavItemLink to="incomeAndExpenseAnalysis">הוצאות מול הכנסות</ReactRouterBootstrap.NavItemLink>
                        <ReactRouterBootstrap.NavItemLink to="transactions">עו"ש</ReactRouterBootstrap.NavItemLink>
                        <ReactRouterBootstrap.NavItemLink to="admin">ניהול</ReactRouterBootstrap.NavItemLink>
                        <ReactRouterBootstrap.NavItemLink to="about">אודות</ReactRouterBootstrap.NavItemLink>
                    </ReactBootstrap.Nav>
                </section>
                <section id="pageContent">
                    <ReactRouter.RouteHandler/>
                </section>
                <footer id="pageFooter">
                    <ReactBootstrap.Grid fluid={true}>
                        <ReactBootstrap.Row>
                            <ReactBootstrap.Col xs={12} md={12} className="copyright center-block text-center">
                                <small>
                                    <strong>כל הזכויות שמורות © לאריק כפיר 2015</strong>
                                </small>
                            </ReactBootstrap.Col>
                        </ReactBootstrap.Row>
                    </ReactBootstrap.Grid>
                </footer>
            </div>
        );
    }
} );

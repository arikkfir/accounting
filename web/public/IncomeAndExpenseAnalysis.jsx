app.IncomeAndExpenseAnalysis = React.createClass({
    getInitialState: function () {
        return {};
    },
    setExpenses: function (data) {
        if (this.isMounted()) {
            this.setState($.extend({}, this.state, {expenses: data}));
        }
    },
    setIncome: function (data) {
        if (this.isMounted()) {
            this.setState($.extend({}, this.state, {income: data}));
        }
    },
    componentDidMount: function () {
        var now = new Date();
        $.get('/api/account/categories/expenses?date=' + now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate(), this.setExpenses);
        $.get('/api/account/categories/income?date=' + now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate(), this.setIncome);
        window.addEventListener('resize', this.applyCharts);
    },
    componentWillUnmount: function () {
        window.removeEventListener('resize', this.applyCharts);
    },
    componentDidUpdate: function () {
        this.applyCharts();
    },
    render: function () {
        return (
            <ReactBootstrap.Grid fluid={true}>
                <ReactBootstrap.Row>
                    <ReactBootstrap.Col md={12}>
                        <ReactBootstrap.Panel header='הוצאות לאורך זמן'>
                            <div id="expensesChart" className="ltr"/>
                        </ReactBootstrap.Panel>
                    </ReactBootstrap.Col>
                </ReactBootstrap.Row>
                <ReactBootstrap.Row>
                    <ReactBootstrap.Col md={12}>
                        <ReactBootstrap.Panel header='הכנסות לאורך זמן'>
                            <div id="incomeChart" className="ltr"/>
                        </ReactBootstrap.Panel>
                    </ReactBootstrap.Col>
                </ReactBootstrap.Row>
            </ReactBootstrap.Grid>
        );
    },
    applyCharts: function () {
        if (this.expensesChart) {
            this.expensesChart.destroy();
        }
        var $expensesChartNode = $(this.getDOMNode()).find('#expensesChart');
        if ($expensesChartNode.length) {
            var expensesChartData = this.aggregateDataForChart(this.state.expenses);
            if (expensesChartData) {
                this.expensesChart = this.createChart($expensesChartNode.get(0), expensesChartData);
            }
        }

        if (this.incomeChart) {
            this.incomeChart.destroy();
        }
        var $incomeChartNode = $(this.getDOMNode()).find('#incomeChart');
        if ($incomeChartNode.length) {
            var incomeChartData = this.aggregateDataForChart(this.state.income);
            if (incomeChartData) {
                this.incomeChart = this.createChart($incomeChartNode.get(0), incomeChartData);
            }
        }
    },
    aggregateDataForChart: function (data) {
        var x = ['x'];
        var series = {};
        for (var date in data) {
            if (data.hasOwnProperty(date)) {
                x.push(date);
                var amountsByCategory = data[date];
                for (var categoryName in amountsByCategory) {
                    if (amountsByCategory.hasOwnProperty(categoryName)) {
                        if (!series[categoryName]) {
                            series[categoryName] = [categoryName];
                        }
                        series[categoryName].push(amountsByCategory[categoryName]);
                    }
                }
            }
        }

        var columns = [x];
        var dataColumns = [];
        for (var seriesName in series) {
            if (series.hasOwnProperty(seriesName)) {
                var hasValue = false;
                for (var i = 1; i < series[seriesName].length; i++) {
                    var value = series[seriesName][i];
                    if (value != 0) {
                        hasValue = true;
                        break;
                    }
                }
                if (hasValue) {
                    columns.push(series[seriesName]);
                    dataColumns.push(seriesName);
                }
            }
        }

        if (columns.length == 1) {
            return null;
        } else {
            return {
                columns: columns,
                dataColumns: dataColumns
            }
        }
    },
    createChart: function (node, aggregatedData) {
        return c3.generate({
            bindto: node,
            data: {
                x: 'x',
                columns: aggregatedData.columns,
                groups: [aggregatedData.dataColumns],
                order: null,
                type: 'area'
            },
            axis: {
                x: {
                    type: 'category'
                },
                y: {
                    tick: {
                        format: function (d) {
                            return numeral(d).format('0,0');
                        }
                    }
                }
            },
            grid: {
                y: {
                    show: true
                }
            },
            padding: {
                left: 60,
                right: 10
            },
            size: {
                height: 300
            },
            transition: {
                duration: 0
            },
            tooltip: {
                grouped: true
            }
        });
    }
});

app.Page = {
    render: function() {
        var title = this.getTitle ? (<h1 className="page-header">{this.getTitle()}</h1>) : (<span/>);
        var contents = this.renderContents ? this.renderContents() : (<span/>);
        return (
            <ReactBootstrap.Grid fluid={true}>
                <ReactBootstrap.Row>
                    <ReactBootstrap.Col md={12}>
                        {title}
                        {contents}
                    </ReactBootstrap.Col>
                </ReactBootstrap.Row>
            </ReactBootstrap.Grid>
        );
    }
};

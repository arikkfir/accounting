var columns = [
    {key: "id", label: "#"},
    {key: "date", label: "תאריך"},
    {key: "comment", label: "הערות"},
    {key: "amount", label: "סכום"},
    {key: "categoryName", label: "קטגוריה"},
    {key: "reference", label: "אסמכתא"}
];

var sorting = [
    {column: 'id', sortFunction: Reactable.Sort.NumericInteger},
    {column: 'date', sortFunction: Reactable.Sort.Date},
    'comment',
    {column: 'amount', sortFunction: Reactable.Sort.Numeric},
    'categoryName',
    'reference'
];

var rowClickHandler = function (txId) {
    this.transitionTo('/transactions/' + txId);
};

var mapTxToRowElement = function (tx) {
    return (
        <Reactable.Tr key={tx.id}
                      className={this.getParams().txId == tx.id ? 'info selected' : tx.amount < 0 ? 'warning' : 'success'}
                      onClick={rowClickHandler.bind(this, tx.id)}>
            <Reactable.Td column="id" data={tx.id}/>
            <Reactable.Td column="date" value={tx.date}>
                <samp>{moment(tx.date).format('DD/MM/YYYY')}</samp>
            </Reactable.Td>
            <Reactable.Td column="comment" data={tx.comment}/>
            <Reactable.Td column="amount" value={tx.amount} className="ltr text-right">
                <samp>{numeral(tx.amount).format('0,0')}</samp>
            </Reactable.Td>
            <Reactable.Td column="categoryName" data={tx.categoryName}/>
            <Reactable.Td column="reference" data={tx.reference}/>
        </Reactable.Tr>
    );
};

app.TransactionsListView = React.createClass({
    mixins: [app.Page, ReactRouter.State, ReactRouter.Navigation],
    getTitle: function () {
        return 'עו"ש';
    },
    getInitialState: function () {
        return {transactions: []};
    },
    setTransactions: function (transactions) {
        if (this.isMounted()) {
            this.setState({transactions: transactions});
        }
    },
    componentDidMount: function () {
        $.get('/api/transactions', this.setTransactions);
    },
    renderContents: function () {
        return (
            <ReactBootstrap.Grid fluid={true}>
                <ReactBootstrap.Row>
                    <ReactBootstrap.Col md={8}>
                        <Reactable.Table columns={columns}
                                         className="table table-bordered table-hover table-condensed"
                                         sortable={sorting}
                                         filterable={['comment', 'reference']}>
                            {this.state.transactions.map(mapTxToRowElement.bind(this))}
                        </Reactable.Table>
                    </ReactBootstrap.Col>
                    <ReactBootstrap.Col md={4}>
                        <ReactRouter.RouteHandler/>
                    </ReactBootstrap.Col>
                </ReactBootstrap.Row>
            </ReactBootstrap.Grid>
        );
    }
});

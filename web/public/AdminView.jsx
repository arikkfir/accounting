app.AdminView = React.createClass( {
    mixins: [ app.Page ],
    getTitle: function() {
        return 'ניהול';
    },
    renderContents: function() {
        return (
            <p>
                מסך זה יציג את כלי הניהול של המערכת, כגון עריכת קטגוריות, מיפויים אוטומטיים של טרנסאקציות, וכד'.
            </p>
        );
    }
} );

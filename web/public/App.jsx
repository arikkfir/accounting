moment.locale( 'he' );

app.routes = (
    <ReactRouter.Route name="app" path="/" handler={app.AppView}>
        <ReactRouter.Route name="dashboard" path="/" handler={app.DashboardView}/>
        <ReactRouter.Route name="transactions" handler={app.TransactionsListView}>
            <ReactRouter.Route name="transaction" path=":txId" handler={app.TransactionView}/>
        </ReactRouter.Route>
        <ReactRouter.Route name="incomeAndExpenseAnalysis" handler={app.IncomeAndExpenseAnalysis}/>
        <ReactRouter.Route name="admin" handler={app.AdminView}/>
        <ReactRouter.Route name="about" handler={app.AboutView}/>
        <ReactRouter.DefaultRoute handler={app.DashboardView}/>
    </ReactRouter.Route>
);

ReactRouter.run( app.routes, function( Handler ) {
    React.render( <Handler/>, document.body );
} );

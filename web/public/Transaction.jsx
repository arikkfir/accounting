app.TransactionView = React.createClass( {
    displayName: "פעולה",
    mixins: [ ReactRouter.State ],
    getInitialState: function() {
        return {
            transaction: {
                id: -1,
                categoryId: -1,
                categoryName: null,
                reference: null,
                date: null,
                amount: null,
                comment: null
            },
            categories: []
        };
    },
    setTransaction: function( tx ) {
        if( this.isMounted() ) {
            this.setState( $.extend( true, {}, this.state, { transaction: tx } ) );
        }
    },
    setCategories: function( categories ) {
        if( this.isMounted() ) {
            this.setState( $.extend( true, {}, this.state, { categories: categories } ) );
        }
    },
    componentDidMount: function() {
        $.get( '/api/transactions/' + this.getParams().txId, this.setTransaction );
        $.get( '/api/categories', this.setCategories );
    },
    componentWillReceiveProps: function() {
        $.get( '/api/transactions/' + this.getParams().txId, this.setTransaction );
    },
    render: function() {
        if( this.state.transaction.id < 0 ) {
            return null;
        } else {
            return (
                <ReactBootstrap.Panel header={this.state.transaction.id + ": " + this.state.transaction.comment}>
                    <form>
                        <ReactBootstrap.Input type="select" label="קטגוריה" defaultValue="-1"
                                              value={this.state.transaction.categoryId ? this.state.transaction.categoryId : -1}>
                            <option key="-1" value={-1}></option>
                            {this.state.categories.map(function(category) {
                                return (<option key={category.id} value={category.id}>{category.name}</option>);
                                })}
                        </ReactBootstrap.Input>
                        <ReactBootstrap.Input type="text" value={this.state.transaction.reference} label="אסמכתא"
                                              readOnly={true}/>
                        <ReactBootstrap.Input type="text" value={this.state.transaction.amount} label="סכום"
                                              readOnly={true}/>
                        <ReactBootstrap.Input type="text" value={this.state.transaction.comment} label="הערה"
                                              readOnly={true}/>
                    </form>
                    <p>תאריך: {this.state.transaction.date}</p>
                </ReactBootstrap.Panel>
            );
        }
    }
} );

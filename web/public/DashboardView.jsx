app.DashboardView = React.createClass({
    getInitialState: function () {
        return {
            balanceForDate: 0,
            thisMonthExpenses: 0,
            thisMonthIncome: 0,
            previousMonthExpenses: 0,
            previousMonthIncome: 0,
            balanceOverTime: {},
            categorySpend: []
        };
    },
    setBalance: function (status) {
        if (this.isMounted()) {
            this.setState(status);
        }
    },
    componentDidMount: function () {
        var now = new Date();
        $.get('/api/account?date=' + now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate(), this.setBalance);
        window.addEventListener('resize', this.applyCharts);
    },
    componentWillUnmount: function () {
        window.removeEventListener('resize', this.applyCharts);
    },
    componentDidUpdate: function () {
        this.applyCharts();
    },
    render: function () {
        var columns = [
            {key: "name", label: "קטגוריה"},
            {key: "thisMonthAmount", label: moment(this.state.date).format('MMMM')},
            {key: "prevMonthAmount", label: moment(this.state.date).subtract(1, 'months').format('MMMM')}
        ];

        var sorting = [
            'name',
            {column: 'thisMonthAmount', sortFunction: Reactable.Sort.Numeric},
            {column: 'prevMonthAmount', sortFunction: Reactable.Sort.Numeric}
        ];

        var mapCategorySpendToRowElement = function (cat) {
            return (
                <Reactable.Tr key={cat.name}>
                    <Reactable.Td column="name" data={cat.name}/>
                    <Reactable.Td column="thisMonthAmount" data={cat.thisMonthAmount}/>
                    <Reactable.Td column="prevMonthAmount" data={cat.prevMonthAmount}/>
                </Reactable.Tr>
            );
        };

        return (
            <ReactBootstrap.Grid fluid={true}>
                <ReactBootstrap.Row>
                    <ReactBootstrap.Col md={4}>
                        <ReactBootstrap.Panel header='עו"ש'>
                            <div id="currentBalanceGauge" className="ltr"/>
                        </ReactBootstrap.Panel>
                    </ReactBootstrap.Col>
                    <ReactBootstrap.Col md={4}>
                        <ReactBootstrap.Panel header={"הוצאות חודש " + moment(this.state.date).format('MMMM')}>
                            {numeral(this.state.thisMonthExpenses).format('0,0')}
                        </ReactBootstrap.Panel>
                        <ReactBootstrap.Panel
                            header={"הוצאות חודש " + moment(this.state.date).subtract(1, 'months').format('MMMM')}>
                            {numeral(this.state.previousMonthExpenses).format('0,0')}
                        </ReactBootstrap.Panel>
                    </ReactBootstrap.Col>
                    <ReactBootstrap.Col md={4}>
                        <ReactBootstrap.Panel header={"הכנסות חודש " + moment(this.state.date).format('MMMM')}>
                            {numeral(this.state.thisMonthIncome).format('0,0')}
                        </ReactBootstrap.Panel>
                        <ReactBootstrap.Panel
                            header={"הכנסות חודש " + moment(this.state.date).subtract(1, 'months').format('MMMM')}>
                            {numeral(this.state.previousMonthIncome).format('0,0')}
                        </ReactBootstrap.Panel>
                    </ReactBootstrap.Col>
                </ReactBootstrap.Row>
                <ReactBootstrap.Row>
                    <ReactBootstrap.Col md={4}>
                        <Reactable.Table className="table table-bordered table-hover table-condensed"
                                         columns={columns}
                                         sortable={sorting}>
                            {this.state.categorySpend.map(mapCategorySpendToRowElement.bind(this))}
                        </Reactable.Table>
                    </ReactBootstrap.Col>
                    <ReactBootstrap.Col md={8}>
                        <ReactBootstrap.Panel header='עו"ש לאורך זמן'>
                            <div id="balanceOverTimeChart" className="ltr"/>
                        </ReactBootstrap.Panel>
                    </ReactBootstrap.Col>
                </ReactBootstrap.Row>
            </ReactBootstrap.Grid>
        );
    },
    applyCharts: function () {
        for (var chart in this.charts) {
            if (this.charts.hasOwnProperty(chart)) {
                chart.destroy();
            }
        }
        this.charts = {};
        this.charts = {
            currentBalanceGauge: this.createCurrentBalanceChart($(this.getDOMNode()), this.state),
            createBalanceOverTimeChart: this.createBalanceOverTimeChart($(this.getDOMNode()), this.state)
        };
    },
    createCurrentBalanceChart: function ($node, state) {
        return c3.generate({
            bindto: $node.find('#currentBalanceGauge').get(0),
            data: {
                columns: [
                    ['data', state.balanceForDate]
                ],
                type: 'gauge'
            },
            gauge: {
                label: {
                    format: function (value) {
                        return numeral(value).format('0,0');
                    }
                },
                expand: true,
                min: -60000,
                max: 60000,
                units: '\u20aa',
                width: 39
            },
            color: {
                pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'],
                threshold: {
                    unit: 'value',
                    max: 20000,
                    values: [-60000, -20000, -5000, 0]
                }
            }
        });
    },
    createBalanceOverTimeChart: function ($node, state) {
        var x = ['x'];
        var y = ['עו"ש'];
        for (var date in state.balanceOverTime) {
            if (state.balanceOverTime.hasOwnProperty(date)) {
                x.push(date);
                y.push(state.balanceOverTime[date]);
            }
        }
        return c3.generate({
            bindto: $node.find('#balanceOverTimeChart').get(0),
            data: {
                x: 'x',
                columns: [
                    x,
                    y
                ],
                type: 'area-spline'
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%d/%m/%Y'
                    }
                },
                y: {
                    min: -60000,
                    max: 60000
                }
            },
            grid: {
                y: {
                    show: true
                }
            },
            padding: {
                left: 50
            },
            legend: {
                show: false
            },
            point: {
                show: false
            }
        });
    }
});

#!/bin/sh

# find application home (copied from: http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in)
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ ${SOURCE} != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
APP_HOME="$( dirname "$DIR" )"

# memory settings
XMS="256m"
XMX="2g"

# write to file and console, or just file?
if [ "${LOG_OUTPUT}" = "" ]; then
    LOG_OUTPUT="logback.xml"
fi

# execute
java \
    -javaagent:$(echo ${APP_HOME}/lib/aspectjweaver-*.jar | tr ' ' ':') \
    -cp ${APP_HOME}/org.arikkfir.accounting.jar:$(echo ${APP_HOME}/lib/*.jar | tr ' ' ':') \
    -Xms${XMS} \
    -Xmx${XMX} \
    -Dapplication.home="${APP_HOME}" \
    -Dlogback.configurationFile=${LOG_OUTPUT} \
    org.arikkfir.accounting.Accounting
